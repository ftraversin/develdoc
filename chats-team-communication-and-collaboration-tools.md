# Chats, team communication and collaboration tools
### Short list with links

1. [![][logo_slack]](https://slack.com)
2. [![][logo_azendoo]](https://azendoo.com)
3. [![][logo_ryver]](https://ryver.com)
4. [![][logo_bitrix24]](https://bitrix24.com)
5. [![][logo_fleep]](https://fleep.io)
6. [![][logo_stride]](https://stride.com)
7. [![][logo_jostle]](https://jostle.me)
8. [![][logo_moxtra]](https://moxtra.com)
9. [![][logo_rocketchat]](https://rocket.chat)
10. [![][logo_hive]](https://hive.com)
11. [![][logo_fuze]](https://fuze.com)
12. [![][logo_glip]](https://glip.com)
13. [![][logo_flock]](https://flock.com)
14. [![][logo_discord]](https://discordapp.com)
15. [![][logo_hangouts]](https://hangouts.google.com)
16. [![][logo_riot]](https://about.riot.im/)
17. [![][logo_zulip]](https://zulipchat.com)
18. [![][logo_gitter]](https://gitter.im)
19. [![][logo_chanty]](https://chanty.com)
20. [![][logo_letschat]](https://github.com/sdelements/lets-chat)
21. [![][logo_mattermost]](https://mattermost.com)
22. [![][logo_friends]](http://moose-team.github.io/friends/)
23. [![][logo_samepage]](https://samepage.io)
24. [![][logo_skype]](https://skype.com/)
25. [![][logo_irccloud]](https://irccloud.com)
26. [![][logo_microsoftteams]](https://products.office.com/en-us/microsoft-teams/group-chat-software)
27. [![][logo_zohocliq]](https://zoho.eu/cliq/)
28. [![][logo_webexteam]](https://www.webex.com/products/teams/index.html)
29. [![][logo_proofhub]](https://proofhub.com)
30. [![][logo_crugo]](https://crugo.com)
31. [![][logo_teamtracker]](https://teamtrackerapp.com)
32. [![][logo_alignus]](https://align.us/features/#social-intranet)
33. [![][logo_mychat]](https://nsoft-s.com/en/aboutmychat.html)
34. [![][logo_troopmessenger]](https://troopmessenger.com)

[logo_slack]: .images/chat-team-communication-tools/slack.png "Slack"
[logo_azendoo]: .images/chat-team-communication-tools/azendoo.png "Azendoo"
[logo_ryver]: .images/chat-team-communication-tools/ryver.png "Ryver"
[logo_bitrix24]: .images/chat-team-communication-tools/bitrix24.png "Bitrix24"
[logo_fleep]: .images/chat-team-communication-tools/fleep.png "Fleep"
[logo_stride]: .images/chat-team-communication-tools/stride.png "Stride"
[logo_jostle]: .images/chat-team-communication-tools/jostle.png "Jostle"
[logo_moxtra]: .images/chat-team-communication-tools/moxtra.png "Moxtra"
[logo_rocketchat]: .images/chat-team-communication-tools/rocketchat.png "RocketChat"
[logo_hive]: .images/chat-team-communication-tools/hive.png "Hive"
[logo_fuze]: .images/chat-team-communication-tools/fuze.png "Fuze"
[logo_glip]: .images/chat-team-communication-tools/glip.png "Glip"
[logo_flock]: .images/chat-team-communication-tools/flock.png "Flock"
[logo_discord]: .images/chat-team-communication-tools/discord.png "Discord"
[logo_hangouts]: .images/chat-team-communication-tools/hangsout.png "Google Hangouts"
[logo_riot]: .images/chat-team-communication-tools/riot.png "Riot"
[logo_zulip]: .images/chat-team-communication-tools/zulip.png "Zulip"
[logo_gitter]: .images/chat-team-communication-tools/gitter.png "Gitter"
[logo_chanty]: .images/chat-team-communication-tools/chanty.png "Chanty"
[logo_letschat]: .images/chat-team-communication-tools/letschat.png "Let's Chat"
[logo_mattermost]: .images/chat-team-communication-tools/mattermost.png "Mattermost"
[logo_friends]: .images/chat-team-communication-tools/friends.png "Friends"
[logo_samepage]: .images/chat-team-communication-tools/samepage.png "Samepage"
[logo_skype]: .images/chat-team-communication-tools/skype.png "Skype"
[logo_irccloud]: .images/chat-team-communication-tools/irccloud.png "IrcCloud"
[logo_microsoftteams]: .images/chat-team-communication-tools/microsoftteams.png "Microsoft Teams"
[logo_zohocliq]: .images/chat-team-communication-tools/cliq.png "Zoho Cliq"
[logo_webexteam]: .images/chat-team-communication-tools/ciscowebexteam.png "Webex Team"
[logo_proofhub]: .images/chat-team-communication-tools/proofhub.png "ProofHub"
[logo_crugo]: .images/chat-team-communication-tools/crugo.png "Crugo"
[logo_teamtracker]: .images/chat-team-communication-tools/teamtracker.png "TeamTracker"
[logo_alignus]: .images/chat-team-communication-tools/alignus.png "Align Us"
[logo_mychat]: .images/chat-team-communication-tools/mychat.png "MyChat"
[logo_troopmessenger]: .images/chat-team-communication-tools/troopmessenger.png "Troop Messenger"