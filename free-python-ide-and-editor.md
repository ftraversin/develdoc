# Free Python IDE and Editor
### Short list with links

1. [JetBrains PyCharm (Community Edition)](https://www.jetbrains.com/pycharm/)
2. [PyScripter](https://sourceforge.net/projects/pyscripter/)
3. [Pyzo](http://pyzo.org)
4. [Thonny](http://thonny.org)
5. [Visual Studio Code](https://code.visualstudio.com)
6. [Atom](https://atom.io)
7. [Eclipse](https://eclipse.org) (+ [PyDev](http://pydev.org))
8. [Geany](https://geany.org)
9. [Spyder](https://github.com/spyder-ide/spyder)
10. [Bluefish](http://bluefish.openoffice.nl/index.html)
11. [Komodo Edit](https://activestate.com/komodo-ide/downloads/edit)
12. [Ninja-Ide](http://ninja-ide.org/)
13. [Eric Ide](https://eric-ide.python-projects.org)
14. [Visual Studio IDE](https://visualstudio.microsoft.com) (+ Python Development Tools)
15. [Vim IDE](https://www.vim.org)
16. [Emacs Editor](https://gnu.org/software/emacs/)
17. [Gedit](https://wiki.gnome.org/Apps/Gedit)
18. [IDLE](https://github.com/python/cpython/tree/3.7/Lib/idlelib/) (Python’s Integrated Development and Learning Environment)
19. [CodeSkulptor](http://py3.codeskulptor.org/) (Online Editor)
20. [Notepad ++](https://notepad-plus-plus.org)


- Other links in: 
  - https://wiki.python.org/moin/PythonEditors
  - https://github.com/vinta/awesome-python#editor-plugins-and-ides
