##### >[develdoc](./readme.md)
# DevelDoc

## Simple collection of documents and links related to various IT topics and programming languages

### Lists
* [Chats, team communication & collaboration tools](./chats-team-communication-and-collaboration-tools.md)
* [Free Python IDE & Editor](./free-python-ide-and-editor.md)
* [Python resources](./python.md)
* [Golang resources](./golang.md)
* [Html resources](./html.md)
