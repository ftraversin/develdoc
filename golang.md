##### >[develdoc](./readme.md)/[golang](./golang.md)

# GOLANG [![][go_logo]](https://golang.org/)
#### Unordered list of Golang Resources
[Communities](#communities) [Books](#books) [Other Resources](#other)

### Communities:
* [GolangBridge](https://golangbridge.org/): [slack](https://gophers.slack.com) gophers community


### Books:
* [Go Cheatsheet](./go/golang_refcard.pdf) (by Refcard)
* [Golang Cheat Sheet](./go/deleted-23330_golang_cheatsheet.pdf) (by [deleted] via cheatography)
* [An Introduction to Programming in Go (GoBook)](./go/gobook.pdf) (by Caleb Doxsey)
* [The Little Go Book](./go/go.pdf) (by Karl Seguin)  
* [Building Web Apps With Go](./go/building-web-apps-with-go.pdf) (by Jeremy Saenz)
* [Webapps in Go, the anti textbook](./go/antitextbookGo.pdf) (by Suraj Patil)
* [Go Web Develpment](./go/Go_Web_Development.pdf) (by Mark Lewin)
* [Go Bootcamp](./go/GoBootcamp.pdf) (by Matt Aimonetti)

### Other resources in:
* [https://medium.com/full-stack-tips/best-golang-books-12a56fc256ab](https://medium.com/full-stack-tips/best-golang-books-12a56fc256ab)
* [https://github.com/dariubs/GoBooks](https://github.com/dariubs/GoBooks)
* [https://senseis.xmp.net/?GoBook](https://senseis.xmp.net/?GoBook)
       
[Up](#golang)

[go_logo]: ./go/go.png "Golang"

