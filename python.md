##### >[develdoc](./readme.md)/[python](./python.md)

# PYTHON [![][python_logo]](https://www.python.org/)
#### Unordered list of Python Resources
[Sites & Blogs](#sites-blogs) [Communities](#communities-) [Books](#books-download-) [Other Resources](#other-resources-in-)

### Sites & blogs
* ![][uk] [bitesofcode.wordpress.com](https://bitesofcode.wordpress.com/): blog molto interessante con articoli e post dedicati alla programmazione nel mondo python e c#.
* ![][uk] [blog.pythonlibrary.org](http://www.blog.pythonlibrary.org/): "The Mouse Vs The Python", ovvero il blog di Mike Driscoll, autore di libri e di vari post su un altro ottimo blog : Python Software Foundation
* ![][uk] [dbader.org](https://dbader.org/): il blog di [Dan Bader](https://twitter.com/dbader_org) (con possibilit� di iscriversi all�interessantissima newsletter).
* ![][uk] [effbot.org](http://effbot.org/): blog minimale ma ricchissimo di contenuti.
* ![][uk] [exercism.io](https://exercism.io/tracks/python): hai bisogno di esercizi per testare le tue competenze nella programmazione? Questo � il posto giusto! (login tramite account github)
* ![][uk] [findnerd.com](findnerd.com): nuovissimo blog tecnico su python di findnerd.com
* ![][uk] [fullstackpython.com](https://www.fullstackpython.com/): ottimo sito dove � possibile trovare tonnellate di risorse relative al mondo python e django. Da seguire anche la relativa pagina facebook e twitter.
* ![][uk] [importpython.com](https://importpython.com/newsletter/): altro sito di newsletter settimanale in cui vi arriveranno informazioni a 360 gradi come articoli, progetti, video, tweets, eventi, tutorial, ecc.
* ![][uk] [jeffknupp.com](https://jeffknupp.com/): blog di Jeff Knupp, l�autore del libro "Writing Idiomatic Python".
* ![][uk] [libraries.io](https://libraries.io/): il miglior sito per trovare qualsiasi tipo di libreria.
* ![][uk] [makeartwithpython.com](https://makeartwithpython.com/): si pu� essere un artista e uno sviluppatore contemporaneamente? Evidentemente s�! Ce lo dimostra questo bel sito di Kirk Kaiser,  in cui si diletta ad utilizzare python per programmi �creativi�. Esempi di codice inclusi.
* ![][uk] [medium.com](https://medium.com/search?q=python): il popolare sito "parente" di twitter e blogger, con interessanti articoli e how-to anche su Python.
* ![][uk] [neopythonic.blogspot.it](http://neopythonic.blogspot.com/). In cima alla lista sicuramente il blog ufficiale di Guido van Rossum!
* ![][uk] [nullege.com](http://nullege.com/): ottimo sito per la ricerca di codice Python. Se necessitate di esempi questo � il posto giusto.
* ![][uk] [pymotw.com](https://pymotw.com/3/): Python Module Of The Week. Ogni settimana una modulo python.
* ![][uk] [pythonfan-blog.tumblr.com](https://pythonfan-blog.tumblr.com/): microblog, dove poter trovare links e articoli interessanti a 360� sul mondo python.
* ![][uk] [pythonforengineers.com](http://pythonforengineers.com/): blog di [Shantnu Tiwari](https://github.com/shantnu) con articoli tecnici e vari how-to.
* ![][uk] [pythonpracticeprojects.com](http://pythonpracticeprojects.com/): se vi serve qualche idea per sviluppare qualche progetto in python e consolidare le vostre abilit� nella programmazione.
* ![][uk] [pythontips.com](https://pythontips.com/): il blog di [Yasoob Khalid](https://github.com/yasoob/), con tutti i trucchi e news su python da questo classico blog.
* ![][uk] [pythonweekly.com](https://www.pythonweekly.com/): newsletter del mondo python. Basta iscriversi e arriveranno puntuali ogni gioved� news, articoli, opportunit� di lavoro e molto altro inerente al mondo python, curate personalmente da Rahul Chaudhary.
* ![][uk] [simpleisbetterthancomplex.com](https://simpleisbetterthancomplex.com/): il blog di [Vitor Freitas](https://github.com/vitorfs) su Python e Django, con interessanti spunti formativi.
* ![][uk] [stackoverflow.com](https://stackoverflow.com/): la pi� grande community al mondo di sviluppatori, dove � possibile imparare, chiedere consulenze, condividere informazioni.
* ![][it] [iprogrammatori.it](https://www.iprogrammatori.it/forum-programmazione/python/): forum in italiano in cui si discute di programmazione e, ovviamente, anche di python nell'apposito canale.
* ![][it] [distillatodipython.blogspot.it](http://distillatodipython.blogspot.com/): blog di  Paolo Di Ieso, molto interessante soprattutto per quel che riguarda la sezione relativa alle risorse.
* ![][it] [python.it](http://www.python.it/blog/): blog ufficiale della comunit� italiana di python.
* ![][it] [okpanico.wordpress.com](https://okpanico.wordpress.com/linguaggi/python/): blog di un gruppo di informatici ed esperti del settore.
* ![][it] [aleax.it](http://www.aleax.it/): non � un vero e proprio blog ma il sito personale di Alex Martelli, fonte di ispirazione per moltissimi pythonisti italiani.
* ![][it] [meccanismocomplesso.org](http://www.meccanismocomplesso.org/python/): non un vero e proprio blog nel senso stretto del termine, ma un sito con una sezione appositamente dedicata a Python in cui � possibile trovare qualche articolo interessante.
* ![][it] [ludusrusso.cc](https://ludusrusso.cc/blog/): il blog del pythonista [Ludus Russo](https://twitter.com/@Ludus89).

                                     
### Communities:
* ![][uk]![][slack] [PySlackers](https://www.pyslackers.com/): [pythondev slack community](https://pythondev.slack.com)
* ![][uk]![][telegram] [Telegram International Chat](https://t.me/Python)
* ![][uk]![][facebook] [Facebook International Group](https://www.facebook.com/groups/python.programmers/)
* ![][it]![][telegram] [Telegram Italian Chat](https://t.me/PythonIta)
* ![][it]![][facebook] [Facebook Italian Group](https://www.facebook.com/groups/python.it/)

### Books (download):
* ![][uk]![][python] [Learning Penetration Testing with Python](./python/9781785282324-LEARNING_PENETRATION_TESTING_WITH_PYTHON.pdf)
* ![][uk]![][python] [Python Essential Reference](./python/Addison.Wesley.Python.Essential.Reference.4th.Edition.Jun.2009.pdf) (by David M. Beazley)
* ![][uk]![][python] [Beginners Python Cheat ](./python/beginners_python_cheat_sheet_pcc_all.pdf)
* ![][uk]![][flask] [Flask](./python/Flask.pdf)
* ![][uk]![][python] [Invent Your Own Computer Games with Python, 2nd Edition](./python/inventwithpython.pdf) (by [Al Sweigart](http://inventwithpython.com/))
* ![][uk]![][kivy] [Kivy Documentation](.python/kivy.pdf) (by [kivy.org](https://kivy.org/#home))
* ![][uk]![][python] [Python 3 Cheat Sheet](.python/mementopython3-english.pdf) (by [Laurent Pointal](https://perso.limsi.fr/pointal/python:memento))
* ![][uk]![][python] [OpenCV-Python Tutorials](.python/opencv-python-tutroals.pdf) (by Alexander Mordvintsev & Abid K)
* ![][uk]![][pyramid] [Pyramid](.python/pyramid.pdf) (by [Chris McDonough](https://twitter.com/chrismcdonough))
* ![][uk]![][qt]![][android] [Writing standalone Qt & Python applications for Android](.python/python_and_qt_for_android.pdf) (by [Martin Kolman](https://twitter.com/M4rtinK))
* ![][it]![][django] [Copia Visione Django](./python/Copia_visione_Django.pdf) (by Marco Beri)
* ![][it]![][python] [Pensare da informatico, Imparare con Python](./python/HowToThink_ITA.pdf) (by Allen Downey, Jeffrey Elkner, Chris Meyers)
      
### Other resources in:
* ![][uk] [Awesome-python](https://awesome-python.com/) [(vinta github resources)](https://github.com/vinta/awesome-python)
* ![][uk] [Awesome-python (libhunt)](https://python.libhunt.com/)
* ![][uk] [Python Crash Course (ehmatthes github cheatsheets)](http://ehmatthes.github.io/pcc/cheatsheets/README.html)

[Up](#python-)

[it]: ./.images/flagita24x24.png "Italian"
[uk]: ./.images/flaguk24x24.png "International"

[facebook]: ./.images/facebook24x24.png "Facebook"
[twitter]: ./.images/twitter24x24.png "Twitter"
[telegram]: ./.images/telegram24x24.png "Telegram"
[slack]: ./.images/slack24x24.png "Slack"

[python]: ./.images/python24x24.png "Python"
[kivy]: ./.images/kivy24x24.png "Kivy"
[qt]: ./.images/qt24x24.png "Qt"
[flask]: ./.images/flask24x24.png "Flask"
[django]: ./.images/django24x24.png "Django"
[pyramid]: ./.images/pyramid24x24.png "Pyramid"
[android]: ./.images/android24x24.png "Android"

[python_logo]: ./python/python.png "Python"
[go_logo]: ./go/go.png "Golang"
[html_logo]: ./html/html.png "Html"



